# ![La Salle BES](http://jcarreras.es/images/lasalle.png)

# ![screenshot](.screenshot.png)

# Descripción
-----------------------

Programa una aplicación simple para puntuar productos

En este ejercicio te encontrarás con un esqueleto de código sin implementar que debes completar. Se trata de una
aplicación para dar de alta productos y puntuarlos, puediendo mostrar luego la media de puntuaciones recibida
por producto.


# Instalación
-----------------------

```
$ make install
```


# Instrucciones
-----------------------

- Prepara la práctica con un `make up`
- Si entras en el directorio `src/examples` verás diferentes ejemplos de uso del cliente Predis. Ejecuta cada uno de ellos (ejemplo: `php 1_key_value.php`) y mira el código para comprender las diferentes maneras de operar con redis
  - `docker-compose run --rm php php /app/examples/1_key_value.php`
  - `docker-compose run --rm php php /app/examples/2_exists.php`
  - `docker-compose run --rm php php /app/examples/3_ttl.php`
  - `docker-compose run --rm php php /app/examples/4_increments.php`
  - `docker-compose run --rm php php /app/examples/5_hash.php`
  
- Abre el fichero `src/tests/ExampleTest.php` para ver qué tienen que hacer las clases `src/App/Product/Create.php` y `src/App/Product/Punctuation.php`
- Puedes ejecutar el comando `make test` para pasar los tests y ver si tu implementación es correcta o no
- El test fallará hasta que no implementes la funcionalidad que falta en las clases `src/App/Product/Create.php` y `src/App/Product/Punctuation.php`


# Desinstalación
-----------------------

```
$ make down
```
