<?php

include( dirname(__FILE__) . '/../vendor/autoload.php');

$client = new Predis\Client(['host' => getenv('REDIS_HOST')]);
if ($client->exists('user:2:surname')) {
	echo 'Surname is ' . $client->get('user:2:surname') . PHP_EOL;
} else {
	echo 'Surname is not defined' . PHP_EOL;
}

$client->del(['user:2:surname']);

if ($client->exists('user:2:surname')) {
	echo 'Surname is ' . $client->get('user:2:surname') . PHP_EOL;
} else {
	echo 'Surname is not defined' . PHP_EOL;
}